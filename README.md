# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

### This test verify issue can be created, searched and edited by user. ###

### How do I get set up? ###

## Configuration ##
* Window: 8.1
* Browser: Firefox v31
* Programing language: Java
* Programing Tool: Eclipse Luna
* Java Version: 1.8.0.25
* External JARs: jar files on selenium-java-2.44.0
* Additional Plug-in for Eclipse: TestNG for Eclipse
* Jira site: https://jira.atlassian.com/browse/TST
## Precondition: ##
* User must be activated.
## Assumption: ##
* For Quick Searching: User know the issue key for searching
* For Advanced Searching: User know the issue key and issue summary for searching
## Steps to verify: ##
1.	Navigate to Atlassian > Jira > A Test Project by Firefox Browser
2.	Login
3.	Create issue
VP: Verify success message is displayed
4.	Search issue through quick searching
VP: Verify issue can be searched through quick searching
VP: Verify issue details after creating
5.	Search issue through advanced searching
6.	Edit issue
VP: Verify issue details after editing
7.	Logout
8.	Close browser

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

### Issues Found ###
Issue # 1: TST-59798 - The wrong error message is displayed for available components after user types used component to the component textbox.