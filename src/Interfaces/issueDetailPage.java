package Interfaces;

public interface issueDetailPage {
	public static final String contentSection = "//*[@id='content']";
	public static final String projectName = "//*[@id='project-name-val']";
	public static final String id = "//*[@id='key-val']";
	public static final String summary = "//*[@id='summary-val']";
	public static final String editButton = "//*[@id='edit-issue']";
	public static final String editSubmitButton = "//*[@id='edit-issue-submit']";
	public static final String type = "//*[@id='type-val']";
	public static final String priority = "//*[@id='priority-val']";
	public static final String dueDate = "//*[@id='due-date']";
	public static final String component = "//*[@id='components-val']";
	public static final String version = "//*[@id='versions-val']";
	public static final String environment = "//*[@id='environment-val']";
	public static final String securityLevel = "//*[@id='security-val']";	
	public static final String description = "//*[@id='description-val']"; 
	public static final String status = "//*[@id='status-val']";
	public static final String resolution = "//*[@id='resolution-val']";
	public static final String assignee = "//*[@id='assignee-val']";
}
