package Interfaces;

public interface menuBar {
	public static final String loginButton = "//*[@class='aui-nav-link login-link']";
	public static final String createButton = "//*[@id='create_link']";
	public static final String quickSearchTextbox = "//*[@id='quickSearchInput']";
	public static final String globalMessage = "//*[@class='global-msg']";
	public static final String issueCreatedKey = "//*[@class='issue-created-key issue-link']";
	public static final String userIcon = "//*[@id='header-details-user-fullname']";
	public static final String logoutButton = "//*[@id='log_out']";
	public static final String issueList = "//*[@id='find_link']";
	public static final String searchForIssues = "//*[@id='issues_new_search_link']";
}
