package Interfaces;

public interface searchIssuePage {
	public static final String viewsStyle = "//*[@id='layout-switcher-button']";
	public static final String listViewStyle = "//*[@data-layout-key='list-view']";
	public static final String detailViewStyle = "//*[@data-layout-key='split-view']";
	public static final String issueLink = "//*[@class='issue-link']";
	public static final String issueNotFoundMessage = "//*[@class='empty-results']";
	public static final String advancedSearchTextbox = "//*[@id='advanced-search']";
	public static final String searchMode = "//*[@class='switcher-item active']";
	public static final String advancedSearchButton = "//*[@class='aui-item aui-button aui-button-subtle search-button']";
	public static final String basicSearchButton = "//*[@class='aui-button aui-button-subtle search-button']";
	
	public static final String issueTable = "//*[@class='issue-table-container']";
	public static final String issueType = "//*[@class='issuetype']";
	public static final String issueId = "//*[@class='issuekey']";
	public static final String issueSummary = "//*[@class='summary']";
	public static final String issueAssignee = "//*[@class='assignee']";
	public static final String issuePriority = "//*[@class='priority']";
	public static final String issueStatus = "//*[@class='status']";
	public static final String issueResolution = "//*[@class='resolution']";
	public static final String issueActionsButton = "//*[@class='issue_actions']";
	public static final String issueEditButton = "//*[@class='aui-list-item-link issueaction-edit-issue']";
	public static final String issueViewButton = "//*[@class='aui-list-item-link issue-link']";
	public static final String issueSearchedLink = "//*[@class='summary']//*[@class='issue-link']";
}
