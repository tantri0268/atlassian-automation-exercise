package Interfaces;

public interface loginPage {
	public static final String usernameLocator = "//*[@id='username']";
	public static final String passwordLocator = "//*[@id='password']";
	public static final String signinButton = "//*[@id='login-submit']";
}
