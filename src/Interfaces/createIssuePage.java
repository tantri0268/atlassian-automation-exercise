package Interfaces;

public interface createIssuePage {
	public static final String projectList = "//*[@class='icon aui-ss-icon noloading drop-menu']";
	public static final String issueTypeList = "//*[@id='issuetype-field']";
	public static final String summaryTextbox = "//*[@id='summary']";
	public static final String securityLevelList = "//*[@id='security']";
	public static final String priorityList = "//*[@id='priority-field']";
	public static final String dueDateTextbox = "//*[@id='duedate']";
	public static final String componentsTextbox = "//*[@id='components-textarea']";
	public static final String componentsItem = "//*[@id='components-multi-select']//*[@class='value-item']";
	public static final String componentsDeleteButton = "//*[@id='components-multi-select']//*[@class='item-delete']";
	public static final String affectsVersionTextbox = "//*[@id='versions-textarea']";
	public static final String affectsItem = "//*[@id='versions-multi-select']//*[@class='value-item']";
	public static final String affectVersionsDeleteButton = "//*[@id='versions-multi-select']//*[@class='item-delete']";
	public static final String fixVerionTextbox = "//*[@id='fixVersions-textarea']";
	public static final String assignList = "//*[@id='assignee-field']";
	public static final String environmentTextbox = "//*[@id='environment']";
	public static final String descriptionTextbox = "//*[@id='description']";
	public static final String selectFileButton = "//*[@class='issue-drop-zone__button aui-button']";
	public static final String createSubmitButton = "//*[@id='create-issue-submit']";
	public static final String cancelButton = "//*[@class='cancel']";
	public static final String removeComponentButton = "//*[@id='components-multi-select''class='item-delete']";
}
