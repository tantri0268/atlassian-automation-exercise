package Interfaces;

public interface editIssuePage extends createIssuePage{
	public static final String editIssueHeader = "//*[@class='jira-dialog-heading']";
	public static final String fieldTab = "//*[@id='aui-uid-6']";
	public static final String moreFields = "//*[@id='aui-uid-7']";
	public static final String yetMoreFields = "//*[@id='aui-uid-8']";
	public static final String updateButton = "//*[@id='edit-issue-submit']";
}
