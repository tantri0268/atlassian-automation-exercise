package Actions;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import Interfaces.menuBar;

public class Logout {
	private WebDriver driver;
	
	public Logout(WebDriver driver)
	{
		this.driver = driver;
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}
	
	public void LogOut()
	{
		driver.findElement(By.xpath(menuBar.userIcon)).click();
		driver.findElement(By.xpath(menuBar.logoutButton)).click();
	}
}
