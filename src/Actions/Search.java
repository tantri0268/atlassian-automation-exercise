package Actions;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

import Interfaces.menuBar;
import Interfaces.searchIssuePage;

public class Search {
	private WebDriver driver;
	
	public Search(WebDriver driver)
	{
		this.driver = driver;
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
	}
	
	public void QuickSearch(String issueID, String issueSummary)
	{
		if(issueID != null)
		{
			driver.findElement(By.xpath(menuBar.quickSearchTextbox)).click();
			driver.findElement(By.xpath(menuBar.quickSearchTextbox)).sendKeys(issueID);
			driver.findElement(By.xpath(menuBar.quickSearchTextbox)).sendKeys(Keys.ENTER);
		}
		else
		{
			driver.findElement(By.xpath(menuBar.quickSearchTextbox)).click();
			driver.findElement(By.xpath(menuBar.quickSearchTextbox)).sendKeys(issueSummary);
			driver.findElement(By.xpath(menuBar.quickSearchTextbox)).sendKeys(Keys.ENTER);
		}
	}
	
	public void AdvancedSearch(String issueID, String issueSummary) throws InterruptedException
	{
		if(driver.findElement(By.xpath(searchIssuePage.searchMode)).getText()=="Advanced")
		{
			driver.findElement(By.xpath(searchIssuePage.searchMode)).click();
		}
		driver.findElement(By.xpath(menuBar.issueList)).click();
		driver.get("https://jira.atlassian.com/issues/?jql=");
		String advancedSearchCommand = "key = '" + issueID + "' AND summary ~ '" + issueSummary + "'";
		driver.findElement(By.xpath(searchIssuePage.advancedSearchTextbox)).click();
		driver.findElement(By.xpath(searchIssuePage.advancedSearchTextbox)).clear();
		driver.findElement(By.xpath(searchIssuePage.advancedSearchTextbox)).sendKeys(advancedSearchCommand);
		driver.findElement(By.xpath(searchIssuePage.advancedSearchButton)).click();
		Thread.sleep(10000);
	}
}
