package Actions;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import Interfaces.*;

public class Edit extends Create{
	public Edit(WebDriver driver) {
		super(driver);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}
	
	public void IssueFromAdvancedSearch(String summary, String securityLevel, String priority, String dueDate, String [] components, String [] versions,String assignee, String environment, String description) throws Exception
	{
		clickButton(By.xpath(searchIssuePage.issueLink));
		Thread.sleep(20000);
		clickButton(By.xpath(issueDetailPage.editButton));
		typeSummary(summary);
		selectSecurityLevel(securityLevel);
		selectPriority(priority);
		typeDueDate(dueDate);
		selectComponents(components);
		selectAffectVersions(versions);
		typeAssignee(assignee);
		typeEnvironment(environment);
		typeDescription(description);
		clickButton(By.xpath(issueDetailPage.editSubmitButton));
	}
}
