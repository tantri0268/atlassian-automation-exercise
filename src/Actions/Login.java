package Actions;

import java.util.concurrent.TimeUnit;

import Interfaces.*;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Login {
	private WebDriver driver;
	
	public Login(WebDriver driver)
	{
		this.driver = driver;
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}
	
	public void typeUsername (String username)
	{
		driver.findElement(By.xpath(loginPage.usernameLocator)).click();
		driver.findElement(By.xpath(loginPage.usernameLocator)).clear();
		driver.findElement(By.xpath(loginPage.usernameLocator)).sendKeys(username);
	}
	
	public void typePassword (String password)
	{
		driver.findElement(By.xpath(loginPage.passwordLocator)).click();
		driver.findElement(By.xpath(loginPage.passwordLocator)).clear();
		driver.findElement(By.xpath(loginPage.passwordLocator)).sendKeys(password);
	}
	
	public Login submitLogin()
	{
		driver.findElement(By.xpath(loginPage.signinButton)).click();
		return this;
	}
	
	public Login As(String username, String password)
	{
		driver.manage().window().maximize();
		driver.findElement(By.xpath(menuBar.loginButton)).click();
		typeUsername(username);
		typePassword(password);
		return submitLogin();
	}
}
