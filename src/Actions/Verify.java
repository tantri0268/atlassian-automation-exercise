package Actions;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import Interfaces.*;

public class Verify {
private WebDriver driver;
	
	public Verify(WebDriver driver)
	{
		this.driver = driver;
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	public void ProjectName(String projectName) throws Exception
	{
		String PROJECTNAME = driver.findElement(By.xpath(issueDetailPage.projectName)).getText();
		Assert.assertTrue(projectName.contains(PROJECTNAME));
	}
	
	public void ID(String Id) throws Exception
	{
		String ID = driver.findElement(By.xpath(issueDetailPage.id)).getText();
		Assert.assertEquals(ID, Id);
	}
	
	public void Summary(String summary) throws Exception
	{
		
		String SUMMARY = driver.findElement(By.xpath(issueDetailPage.summary)).getText();
		Assert.assertEquals(SUMMARY, summary);
	}
	
	public void SecurityLevel(String securityLevel) throws Exception
	{
		if(securityLevel == "" || securityLevel == "None")
		{
			By element = By.xpath(issueDetailPage.securityLevel);
			boolean present = IsElementPresent(element);
			Assert.assertEquals(present, false);
		}
		else
		{
			String SECURITY = driver.findElement(By.xpath(issueDetailPage.securityLevel)).getText();
			Assert.assertTrue(SECURITY.contains(securityLevel));
		}
	}
	
	public void Priority(String priority) throws Exception
	{
		String PRIORITY = driver.findElement(By.xpath(issueDetailPage.priority)).getText();
		Assert.assertEquals(PRIORITY, priority);
	}
	
	public void Type(String issueType) throws Exception
	{
		String TYPE = driver.findElement(By.xpath(issueDetailPage.type)).getText();
		Assert.assertEquals(TYPE, issueType);
	}
	
	public void DueDate(String dueDate) throws Exception
	{
		String DUEDATE = driver.findElement(By.xpath(issueDetailPage.dueDate)).getText();
		Assert.assertEquals(DUEDATE, dueDate);
	}
	
	public void Component(String [] components) throws Exception
	{
		String COMPONENT = driver.findElement(By.xpath(issueDetailPage.component)).getText();
		if(components.length==0)
		{
			Assert.assertEquals(COMPONENT,"None");
		}
		for(String component : components)
		{
			Assert.assertTrue(COMPONENT.toString().contains(component));
		}
	}
	
	public void Version(String [] versions) throws Exception
	{
		String VERSION = driver.findElement(By.xpath(issueDetailPage.version)).getText();
		if(versions.length==0)
		{
			Assert.assertEquals(VERSION,"None");
		}
		else
		{
			for(String version : versions)
			{
				Assert.assertTrue(VERSION.toString().contains(version));
			}
		}
	}
 
	public void Assignee(String assignee) throws Exception
	{
		String ASSIGNEE = driver.findElement(By.xpath(issueDetailPage.assignee)).getText();
		if(assignee == "")
			Assert.assertEquals(ASSIGNEE, "Unassigned");
		else
			Assert.assertEquals(ASSIGNEE, assignee);
	}
	
	public void Environment(String environment) throws Exception
	{
		if(environment == null)
		{
			By element = By.xpath(issueDetailPage.environment);
			boolean present = IsElementPresent(element);
			Assert.assertEquals(present, false);
		}
		else
		{
			String ENVIRONMENT = driver.findElement(By.xpath(issueDetailPage.environment)).getText();
			Assert.assertEquals(ENVIRONMENT, environment);
		}
	}
	
	public void Description(String description) throws Exception
	{
		String DESCRIPTION = driver.findElement(By.xpath(issueDetailPage.description)).getText();
		if(description == "")
			Assert.assertEquals(DESCRIPTION, "Click to add description");
		else
			Assert.assertEquals(DESCRIPTION, description);
	}
	
	public void Status(String status) throws Exception
	{
		String STATUS = driver.findElement(By.xpath(issueDetailPage.status)).getText();
		if(status == "")
			Assert.assertEquals(STATUS, "OPEN");
		else
			Assert.assertEquals(STATUS, status.toUpperCase());
	}
	
	public void Resolution(String resolution) throws Exception
	{
		String RESOLUTION = driver.findElement(By.xpath(issueDetailPage.resolution)).getText();
		if(resolution == "")
			Assert.assertEquals(RESOLUTION, "Unresolved");
		else
			Assert.assertEquals(RESOLUTION, resolution);
	}
	
	public boolean IsElementPresent(By element)
	{
		boolean present;
		try
		{
			driver.findElement(element);
			present = true;
		}
		catch (Exception e)
		{
			present = false;
		}
		return present;
	}
	
	public void ElementExisted(By element, boolean isExisted) throws Exception
	{
		boolean present = IsElementPresent(element);
		Assert.assertEquals(present, isExisted);
	}
	
	public void IssueDetails(String projectName,String id, String type, String summary, 
	String securityLevel, String priority, String dueDate, String [] components, String [] versions,
	String assignee, String environment, String description, String status, String resolution) throws Exception
	{
		String issueUrl = "https://jira.atlassian.com/browse/" + id; 
		driver.get(issueUrl);
		ProjectName(projectName);
		ID(id);
		Type(type);
		Summary(summary);
		SecurityLevel(securityLevel);
		Priority(priority);
		DueDate(dueDate);
		Component(components);
		Version(versions);
		Environment(environment);
		Description(description);
		Status(status);
		Resolution(resolution);
	}
	
//	public void TypeListView(String type) throws Exception
//	{
//		
//		String TYPE = driver.findElement(By.xpath(searchIssuePage.issueType)).getAttribute("alt");
//		Assert.assertEquals(TYPE, type);
//	}
//	
//	public void IdListView(String id) throws Exception
//	{
//		String ID = driver.findElement(By.xpath(searchIssuePage.issueId)).getText();
//		Assert.assertEquals(ID, id);
//	}
//	
//	public void SummaryListView(String summary) throws Exception
//	{
//		String SUMMARY = driver.findElement(By.xpath(searchIssuePage.issueSummary)).getText();
//		Assert.assertEquals(SUMMARY, summary);
//	}
//	
//	public void AssigneeListView(String assignee) throws Exception
//	{
//		String ASSIGNEE = driver.findElement(By.xpath(searchIssuePage.issueAssignee)).getText();
//		if(assignee == "")
//			Assert.assertEquals(ASSIGNEE, "Unassigned");
//		else
//			Assert.assertEquals(ASSIGNEE, assignee);
//	}
//	
//	public void PriorityListView(String priority) throws Exception
//	{
//		String PRIORITY = driver.findElement(By.xpath(searchIssuePage.issuePriority)).getAttribute("alt");
//		Assert.assertEquals(PRIORITY, priority);
//	}
//	
//	public void StatusListView(String status) throws Exception
//	{
//		String STATUS = driver.findElement(By.xpath(searchIssuePage.issueStatus)).getAttribute("alt");
//		Assert.assertEquals(STATUS, status);
//	}
//	
//	public void ResolutionListView(String resolution) throws Exception
//	{
//		String RESOLUTION = driver.findElement(By.xpath(searchIssuePage.issueResolution)).getText();
//		Assert.assertEquals(RESOLUTION, resolution);
//	}
//	
//	public void IssueDetailsFromAdvancedSearch(String type, String id, String summary, String assignee, String priority, String status, String resolution) throws Exception
//	{
//		driver.findElement(By.xpath(searchIssuePage.viewsStyle)).click();
//		driver.findElement(By.xpath(searchIssuePage.listViewStyle)).click();
//		TypeListView(type);
//		IdListView(id);
//		SummaryListView(summary);
//		AssigneeListView(assignee);
//		PriorityListView(priority);
//		StatusListView(status);
//		ResolutionListView(resolution);
//	}
}
