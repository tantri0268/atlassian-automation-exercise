package Actions;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import Interfaces.*;

public class Create {
	private WebDriver driver;
	
	public Create(WebDriver driver)
	{
		this.driver = driver;
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}
	
	public void selectProject(String projectName)
	{
		driver.findElement(By.xpath(createIssuePage.projectList)).click();
		driver.findElement(By.xpath(createIssuePage.projectList)).sendKeys(Keys.DELETE);
		driver.findElement(By.xpath(createIssuePage.projectList)).sendKeys(projectName);
	}
	
	public void selectIssueType(String type) throws Exception
	{
		driver.findElement(By.xpath(createIssuePage.issueTypeList)).click();
		driver.findElement(By.xpath(createIssuePage.issueTypeList)).sendKeys(Keys.DELETE);
		driver.findElement(By.xpath(createIssuePage.issueTypeList)).sendKeys(type);
		driver.findElement(By.xpath(createIssuePage.issueTypeList)).sendKeys(Keys.ENTER);
		Thread.sleep(10000);
	}

	public void typeSummary(String summary)
	{
		driver.findElement(By.xpath(createIssuePage.summaryTextbox)).click();
		driver.findElement(By.xpath(createIssuePage.summaryTextbox)).clear();
		driver.findElement(By.xpath(createIssuePage.summaryTextbox)).sendKeys(summary);
	}
		
	public void selectSecurityLevel(String securityLevel)
	{
		driver.findElement(By.xpath(createIssuePage.securityLevelList)).click();
		WebElement securityList = driver.findElement(By.xpath(createIssuePage.securityLevelList));
		Select security = new Select(securityList);
		security.selectByVisibleText(securityLevel);
		driver.findElement(By.xpath(createIssuePage.securityLevelList)).sendKeys(Keys.ENTER);
	}
	
	public void selectPriority(String priority)
	{
		driver.findElement(By.xpath(createIssuePage.priorityList)).click();
		driver.findElement(By.xpath(createIssuePage.priorityList)).sendKeys(Keys.DELETE);
		driver.findElement(By.xpath(createIssuePage.priorityList)).sendKeys(priority);
	}
	
	public void typeDueDate(String dueDate)
	{
		driver.findElement(By.xpath(createIssuePage.dueDateTextbox)).click();
		driver.findElement(By.xpath(createIssuePage.dueDateTextbox)).clear();
		driver.findElement(By.xpath(createIssuePage.dueDateTextbox)).sendKeys(dueDate);
	}
	
	public void selectComponents(String [] components)
	{
		while(IsElementPresent(By.xpath(createIssuePage.componentsItem))==true)
		{
		driver.findElement(By.xpath(createIssuePage.componentsDeleteButton)).click();
		}
		for (int i=0;i<components.length;i++)
		{
			driver.findElement(By.xpath(createIssuePage.componentsTextbox)).click();
			driver.findElement(By.xpath(createIssuePage.componentsTextbox)).sendKeys(components[i]);
			driver.findElement(By.xpath(createIssuePage.componentsTextbox)).sendKeys(Keys.ENTER);
		}
	}
	
	public void selectAffectVersions(String [] versions)
	{
		while(IsElementPresent(By.xpath(createIssuePage.affectsItem))==true)
		{
		    driver.findElement(By.xpath(createIssuePage.affectVersionsDeleteButton)).click();
		}
		for (int i=0;i<versions.length;i++)
		{
			driver.findElement(By.xpath(createIssuePage.affectsVersionTextbox)).click();
			driver.findElement(By.xpath(createIssuePage.affectsVersionTextbox)).sendKeys(versions[i]);
			driver.findElement(By.xpath(createIssuePage.affectsVersionTextbox)).sendKeys(Keys.ENTER);
		}
	}
	
	public void typeAssignee(String assignee)
	{
		if(assignee == null)
		{
			driver.findElement(By.xpath(createIssuePage.assignList)).click();
			driver.findElement(By.xpath(createIssuePage.assignList)).sendKeys("Unassigned");	
		}
		else
		{
			driver.findElement(By.xpath(createIssuePage.assignList)).click();
			driver.findElement(By.xpath(createIssuePage.assignList)).sendKeys(assignee);
		}
	}
	
	public void typeEnvironment(String environment)
	{
		driver.findElement(By.xpath(createIssuePage.environmentTextbox)).click();
		driver.findElement(By.xpath(createIssuePage.environmentTextbox)).clear();
		driver.findElement(By.xpath(createIssuePage.environmentTextbox)).sendKeys(environment);
	}
	
	public void typeDescription(String description)
	{
		driver.findElement(By.xpath(createIssuePage.descriptionTextbox)).click();
		driver.findElement(By.xpath(createIssuePage.descriptionTextbox)).clear();
		driver.findElement(By.xpath(createIssuePage.descriptionTextbox)).sendKeys(description);
	}
	
	public void clickButton(By element) throws InterruptedException
	{
		WebElement Element = driver.findElement(element);
		Element.click();
	}
	
	public boolean IsElementPresent(By element)
	{
		boolean present;
		try
		{
			driver.findElement(element);
			present = true;
		}
		catch (Exception e)
		{
			present = false;
		}
		return present;
	}
	
	public void Issue(String projectName, String type, String summary, 
	String securityLevel, String priority, String dueDate, String [] components, String [] versions,
	String assignee, String environment, String description) throws Exception
	{
		clickButton(By.xpath(menuBar.createButton));
		selectProject(projectName);
		selectIssueType(type);
		typeSummary(summary);
		selectSecurityLevel(securityLevel);
		selectPriority(priority);
		typeDueDate(dueDate);
		selectComponents(components);
		selectAffectVersions(versions);
		typeAssignee(assignee);
		typeEnvironment(environment);
		typeDescription(description);
		clickButton(By.xpath(createIssuePage.createSubmitButton));
	}
}
