package Actions;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import Interfaces.*;

public class Get {
private WebDriver driver;
	
	public Get(WebDriver driver)
	{
		this.driver = driver;
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}
	
	public String IssueID()
	{
		String IssueID = driver.findElement(By.xpath(menuBar.issueCreatedKey)).getAttribute("data-issue-key");
		return IssueID;
	}
}