package Tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import Actions.*;
import Interfaces.*;

public class TC01_VerifyIssueCanBeCreatedSearchedAndEdited{
	
	WebDriver driver = new FirefoxDriver();
	Login login = new Login(driver);
	Logout logout = new Logout(driver);
	Create create = new Create(driver);
	Edit edit = new Edit(driver);
	Search search = new Search(driver);
	Verify verify = new Verify(driver);
	Get get = new Get(driver);
	
	String url = new String ("https://jira.atlassian.com/browse/TST");
	String username = "AtlasSelenium@gmail.com";
	String password = "saigon123";
	String projectName = "A Test Project (TST)";
	String type = "Bug";
	String summary = "This is the automated creation issue";
	String securityLevel = "Reporter and developers";
	String priority = "Major";
	String dueDate = "14/Dec/14";
	String [] components = new String [] {"Component 1","Component 2"};
	String [] versions = new String [] {"A version"};
	String assignee;
	String environment;
	String description = "This is a test";
	String issueID;
	String status = "Open";
	String resolution = "Unresolved";
	
	String newSummary = "This is edited summary";
	String newSecurityLevel = "None";
	String newPriority = "Minor";
	String newDueDate = "15/Dec/14";
	String [] newComponents = new String [] {"Component 1"};
	String [] newVersions = new String [] {"A version"};
	String newEnvironment = "This is new environment";
	String newDescription = "This is new description";

	@BeforeMethod
	public void BeforeMethod() throws Exception
	{
//	Steps:
//		1. Navigate to Atlassian > Jira > A Test Project by Firefox Browser
		driver.get(url);
//		2. Login to site
		login.As(username, password);
//		3. Create issue
		create.Issue(projectName, type, summary, securityLevel, priority, dueDate, components, versions, assignee, environment, description);
	}
	
	@Test
	public void VerifyIssueCanBeCreatedSearchedAndEdited() throws Exception
	{
//		VP: Verify success message is displayed
		verify.ElementExisted(By.xpath(menuBar.globalMessage), true);
		verify.ElementExisted(By.xpath(menuBar.issueCreatedKey), true);
		issueID = get.IssueID();
//		4. Search issue through quick searching
		search.QuickSearch(issueID, summary);
//		VP: Verify issue can be searched through quick searching
		verify.ElementExisted(By.xpath(searchIssuePage.issueNotFoundMessage), false);
//		VP: Verify issue details after creating
		verify.IssueDetails(projectName, issueID, type, summary, securityLevel, priority, dueDate, components, versions, assignee, environment, description, status, resolution);
//		5. Search issue through advanced searching
		search.AdvancedSearch(issueID, summary);
//		6. Edit issue
		edit.IssueFromAdvancedSearch(newSummary, newSecurityLevel, newPriority, dueDate, newComponents, newVersions, assignee, environment, description);
//		VP: Verify issue details after editing
		verify.IssueDetails(projectName, issueID, type, newSummary, newSecurityLevel, newPriority, dueDate, newComponents, newVersions, assignee, environment, description, status, resolution);
	}
	
	@AfterMethod
	public void AfterMethod()
	{
//		7. Logout
		logout.LogOut();
//		8. Close browser
		driver.close();
	}
}
